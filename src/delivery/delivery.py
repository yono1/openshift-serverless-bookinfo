import os
import uuid
from datetime import datetime
import logging
import json
from flask import Flask,request,make_response
from kafka import KafkaProducer

# instantiate the API
app = Flask(__name__)

# 環境変数を取得
KAFKA_TOPIC = "delivery" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVERS = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVERS") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVERS")
SERVICE_PORT = "9080" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")
PRODUCER_MODE = False if (os.environ.get("PRODUCER_MODE") is None) else os.getenv('PRODUCER_MODE', 'False').lower() == 'true'
SUCCESS_STATUS_CODE = "200" if (os.environ.get("SUCCESS_STATUS_CODE") is None) else os.environ.get("SUCCESS_STATUS_CODE")
ERROR_STATUS_CODE = "500" if (os.environ.get("ERROR_STATUS_CODE") is None) else os.environ.get("ERROR_STATUS_CODE")

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)


# delivery topicを生成する
def produce(_id: int, _product_id: int, _state: str, _user: str):
    # Kafka Prodcuer initialize
    producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVERS])

    # トピックへ送信するデータを構成
    value = { 'created_at': datetime.now().strftime('%Y/%m/%d %H:%M:%S'), 'id': _id, 'product_id': _product_id, 'status': _state, 'user': _user }
    log.info(f'Sending message with value -> {value}')
    value_json = json.dumps(value).encode('utf-8')

    try:
        # トピック送信
        producer.send(KAFKA_TOPIC, value_json)
    finally:
        producer.close()

# クラウドイベントの受信し、注文状態としてDELIVERY_SUCCESSを返却
@app.route("/", methods=["POST"])
def receive_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")
    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    if _user:
        # ユーザが存在する場合は配送成功
        _state = "DELIVERY_SUCCESS"
        status_code = int(SUCCESS_STATUS_CODE)
    else:
        # ユーザが存在しない場合は配送失敗
        _state = "DELIVERY_FAILED [USER is Not Found]"
        status_code = int(ERROR_STATUS_CODE)
        
    if PRODUCER_MODE:
        # Kafka Topicへ直接Publish
        produce(_order_id, _product_id, _state, _user)
        # PRODUCER_MODEがTrueの場合、Kafka Topicを送信し、HTTPレスポンスのデータは返さない
        return "", status_code
    else:
        # PRODUCER_MODEがFalseの場合、別のCloudEvents形式のイベントデータをHTTPレスポンスとして応答
        event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': _state, 'user': _user }
        response = make_response( json.dumps(value).encode('utf-8') )
        response.headers["Ce-Id"] = str(uuid.uuid4())
        response.headers["Ce-Source"] = "dev.knative.serving#delivery"
        response.headers["Ce-specversion"] = "1.0"
        response.headers["Ce-Type"] = "cloudevent.event.type"
        return response, status_code

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))