import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

DB_HOST = "localhost" if (os.environ.get("DB_HOST") is None) else os.environ.get("DB_HOST") 
DB_USERNAME = "changeme" if (os.environ.get("DB_USERNAME") is None) else os.environ.get("DB_USERNAME") 
DB_PASSWORD = "changeme" if (os.environ.get("DB_PASSWORD") is None) else os.environ.get("DB_PASSWORD") 
DB_NAME = "orders" if (os.environ.get("DB_NAME") is None) else os.environ.get("DB_NAME") 


engine = create_engine(f'mysql+pymysql://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:3306/{DB_NAME}')

db_session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=engine,
    )
)
Base = declarative_base()