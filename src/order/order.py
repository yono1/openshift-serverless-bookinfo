import os
import uuid
from datetime import datetime
import logging
import json
from flask import Flask, request,make_response

# import Kafka modules
from kafka import KafkaProducer

#import DB modules
from model import Order
from db import db_session

# instantiate the API
app = Flask(__name__)

# 環境変数の取得
KAFKA_TOPIC = "orders" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVERS = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVERS") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVERS")
SERVICE_PORT = "9080" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")
PRODUCER_MODE = False if (os.environ.get("PRODUCER_MODE") is None) else os.getenv('PRODUCER_MODE', 'False').lower() == 'true'

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)


# Kafka Topicの送信
def produce( _id: int, _product_id: int, _state: str, _user: str):
    # Kafka Prodcuer initialize
    producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVERS])

    # トピックへ送信するデータを構成
    value = { 'created_at': datetime.now().strftime('%Y/%m/%d %H:%M:%S'), 'id': _id, 'product_id': _product_id, 'status': _state, 'user': _user }
    log.info(f'Sending message with value -> {value}')
    value_json = json.dumps(value).encode('utf-8')

    try:
        # トピック送信
        producer.send(KAFKA_TOPIC, value_json)
    finally:
        producer.close()

# ordersテーブルから最新行のみを取得
def select_db(_product_id: int):
    order = db_session.query(Order).filter(Order.product_id == _product_id).order_by(Order.created_at.desc()).limit(1).first()
    db_session.commit()
    return order

# ordersテーブルへイベント情報をINSERT
def insert_db(_product_id: int, _state: str) -> None:
    order = Order()
    order.product_id = _product_id
    order.status = _state
    db_session.add(order)
    db_session.commit()

# ordersテーブルの対象レコードを更新
def update_db(_id: int, _product_id: int, _state: str) -> None:
    order = db_session.query(Order).filter(Order.id == _id).first()
    order.status = _state
    db_session.add(order)
    db_session.commit()

# 注文状態を参照
@app.route("/orders/<_product_id>", methods=["GET"])
def get_order_state(_product_id: int):
    # Orders DBから書籍の注文状態をSELECT
    result = select_db(_product_id)

    # SELECT結果が存在する場合、その結果を返す
    if hasattr(result, "id"):
        return_value = { 
            "id": result.id,
            "product_id": result.product_id,
            "status": result.status,
            "created_at": result.created_at,
            "updated_at": result.updated_at
            }
    else:
        return_value = { 
            "msg": f"data of product_id {_product_id} is Not Found"
            }

    return return_value

# 注文受付処理を実行
@app.route("/orders/<_product_id>", methods=["POST"])
def order_create(_product_id: int):
    # POSTデータを取得
    data = json.loads( request.data )
    _user = str(data["user"])
    _product_id = int(data["product_id"])

    log.info(f"POST data, product_id:{_product_id} user:{_user}")

    _state = "ORDER_CREATED"

    # レコードの挿入
    insert_db(_product_id, _state)

    # Order DBの最新行を参照
    result = select_db(_product_id)

    if hasattr(result, "id"):
        # Kafka Topicへ注文イベントをPublish
        produce(result.id, _product_id, _state, _user)

        return_value = { 
            "id": result.id,
            "product_id": result.product_id,
            "status": result.status,
            "created_at": result.created_at,
            "updated_at": result.updated_at,
            "user": _user
            }
    else:
        return_value = { 
            "msg": f"data of product_id {_product_id} is Not Found"
            }

    return return_value


# イベントを受信し、orderDBのステータスを更新する
@app.route("/", methods=["POST"])
def receive_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")
    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    # 注文のステータスを更新する
    update_db(_order_id, _product_id, _state)

    if PRODUCER_MODE:
        # Kafka Topicへ直接Publish
        produce(_order_id, _product_id, _state, _user)
        # PRODUCER_MODEがTrueの場合、Kafka Topicを送信し、HTTPレスポンスとしてイベントデータは返さない
        return "", 200
    else:
        # PRODUCER_MODEがFalseの場合、新たなイベントとしてHTTPレスポンス
        event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': _state, 'user': _user }
        response = make_response( json.dumps(value).encode('utf-8') )
        response.headers["Ce-Id"] = str(uuid.uuid4())
        response.headers["Ce-Source"] = "dev.knative.serving#order"
        response.headers["Ce-specversion"] = "1.0"
        response.headers["Ce-Type"] = "cloudevent.event.type"
        return response, 200


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))