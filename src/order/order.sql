USE orders;

CREATE TABLE orders(
    id bigint primary key auto_increment,
    product_id bigint not null,
    status varchar(255) default 'PENDING',
    created_at datetime,
    updated_at datetime
);