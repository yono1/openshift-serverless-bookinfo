import os
import uuid
from datetime import datetime
import logging
import json
from flask import Flask, request,make_response
from kafka import KafkaProducer

# DB modules
from model import Stock
from db import db_session

# instantiate the API
app = Flask(__name__)

# 環境変数の取得
KAFKA_TOPIC = "stock" if (os.environ.get("KAFKA_TOPIC") is None) else os.environ.get("KAFKA_TOPIC")
KAFKA_BOOTSTRAP_SERVERS = "localhost:9092" if (os.environ.get("KAFKA_BOOTSTRAP_SERVERS") is None) else os.environ.get("KAFKA_BOOTSTRAP_SERVERS")
SERVICE_PORT = "9080" if (os.environ.get("SERVICE_PORT") is None) else os.environ.get("SERVICE_PORT")
PRODUCER_MODE = False if (os.environ.get("PRODUCER_MODE") is None) else os.getenv('PRODUCER_MODE', 'False').lower() == 'true'

# Loggerを初期化
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)

# stock topicを生成する
def produce(_id: int, _product_id: int, _state: str, _user:str):
    # Kafka Prodcuer initialize
    producer = KafkaProducer(bootstrap_servers=[KAFKA_BOOTSTRAP_SERVERS])

    # トピックへ送信するデータを構成
    value = { 'created_at': datetime.now().strftime('%Y/%m/%d %H:%M:%S'), 'id': _id, 'product_id': _product_id, 'status': _state, 'user': _user }
    log.info(f'Sending message with value -> {value}')
    value_json = json.dumps(value).encode('utf-8')

    try:
        # トピック送信
        producer.send(KAFKA_TOPIC, value_json)
    finally:
        producer.close()

# stockテーブルから当該product_idのレコードを取得
def select_db(_product_id: int):
    stock = db_session.query(Stock).filter(Stock.product_id == _product_id).first()
    db_session.commit()
    return stock

# 在庫情報を更新する
def update_db(_product_id: int, _state: str) -> None:
    stock = db_session.query(Stock).filter(Stock.product_id == _product_id).first()
    if stock.count > 0:
        log.info(f'update of stock counts for {stock.count}')

        # 在庫数を1減らしてDBを更新
        stock.count = Stock.count - 1
        db_session.add(stock)
        db_session.commit()

        _state = "STOCK_SUCCESS"
        status_code = 200
        
    else:
        log.info(f'Stock is nothing. so skip update stock table.')
        _state = 'ORDER_FAILED[NO STOCK]'
        status_code = 500

    return _state, status_code

# 在庫情報をロールバックする
def rollback_stock(_id: int, _product_id: int, _state: str, _user: str) -> None:
    stock = db_session.query(Stock).filter(Stock.product_id == _product_id).first()
    
    # 在庫数を1つ増やしてDBを更新
    stock.count = Stock.count + 1
    log.info(f'rollback of stock counts for {stock.count}')
    db_session.add(stock)
    db_session.commit()
    _state = "ORDER_FAILED"
    
    return _state

# 在庫状態を参照
@app.route("/stock/<_product_id>", methods=["GET"])
def get_stock_state(_product_id: int):
    # Stock DBから書籍の注文状態をSELECT
    result = select_db(_product_id)

    # SELECT結果が存在する場合、その結果を返す
    if hasattr(result, "id"):
        return_value = { 
            "id": result.id,
            "product_id": result.product_id,
            "count": result.count
            }
    else:
        return_value = { 
            "msg": f"data of product_id {_product_id} is Not Found"
            }

    return return_value

# クラウドイベントを受信し、在庫情報を更新する
@app.route("/", methods=["POST"])
def receive_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")
    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    # 在庫情報を更新してイベントとして記録する
    _state, status_code = update_db(_product_id, _state)

    if PRODUCER_MODE:
        # Kafka Topicへ直接Publish
        produce(_order_id, _product_id, _state, _user)
        # PRODUCER_MODEがTrueの場合、Kafka Topicを送信し、HTTPレスポンスとしてイベントデータは返さない
        return "", status_code
    else:
        # PRODUCER_MODEがFalseの場合、新たなイベントとしてHTTPレスポンス
        event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': _state, 'user': _user }
        response = make_response( json.dumps(value).encode('utf-8') )
        response.headers["Ce-Id"] = str(uuid.uuid4())
        response.headers["Ce-Source"] = "dev.knative.serving#stock"
        response.headers["Ce-specversion"] = "1.0"
        response.headers["Ce-Type"] = "cloudevent.event.type"
        return response, status_code


# クラウドイベントの受信し、Stock DBの更新をロールバック(在庫情報を1つ増やす)する
@app.route("/rollback", methods=["POST"])
def rollback_cloudevents():

    # クラウドイベントを受信する    
    log.info(f"received cloudevents data: {request.data}")
    event = json.loads( request.data )
    _order_id = int(event["id"])
    _product_id = int(event["product_id"])
    _state = str(event["status"])
    _user = str(event["user"])

    # 在庫情報を更新してイベントとして記録する
    _state = rollback_stock(_order_id, _product_id, _state, _user)

    if PRODUCER_MODE:
        # Kafka TopicへPublish
        produce(_order_id, _product_id, _state, _user)
        # PRODUCER_MODEがTrueの場合、HTTPレスポンスのデータは返さない
        return "", 200
    else:
        # PRODUCER_MODEがOFalseの場合、別のCloudEvents形式のイベントデータをHTTPレスポンスとして応答
        event_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        value = { 'created_at': event_time, 'id': _order_id, 'product_id': _product_id, 'status': _state, 'user': _user }
        response = make_response( json.dumps(value).encode('utf-8') )
        response.headers["Ce-Id"] = str(uuid.uuid4())
        response.headers["Ce-Source"] = "dev.knative.serving#stock_rollback"
        response.headers["Ce-specversion"] = "1.0"
        response.headers["Ce-Type"] = "cloudevent.event.type"
        return response, 200


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(SERVICE_PORT))