from sqlalchemy import Column, BigInteger, Integer
from db import Base

class Stock(Base):
    __tablename__ = 'stock'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    product_id = Column(BigInteger, nullable=False)
    count = Column(Integer, default=5)