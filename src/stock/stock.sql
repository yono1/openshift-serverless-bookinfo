USE stock;

CREATE TABLE stock(
    id bigint primary key auto_increment,
    product_id bigint not null,
    count int default 10
);