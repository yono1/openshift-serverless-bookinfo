# OpenShift Serverless Bookinfo

## 1. はじめに

本リポジトリは、OpenShift Serverlessを体験するデモです。
Istioのサンプルアプリケーションである「Bookinfo」とインプレス社出版「[Knative 実践ガイド](https://book.impress.co.jp/books/1122101070)」で使用している「Bookorder」を使用して、OpenShift Serverlessの基本機能を体験します。

## 2. 環境要件
### 使用するCLIツール
- oc
- [helm](https://helm.sh/ja/docs/intro/install/)
- [envsubst](https://github.com/a8m/envsubst)
- [tkn](https://tekton.dev/docs/cli)
- [kn](https://knative.dev/docs/client/install-kn)

### テスト済みの環境
- OpenShift 4.12

## 3. Getting started

### 3-1. 使用するプロジェクトを作成します。
```
## アプリケーション実行用のproject
oc new-project bookinfo

## Kafkaリソースを実行する用途のproject
oc new-project kafka
```

### 3-2. Operatorをインストールします。
#### 3-2-1. インストールするOperator
- AMQ Operator
- OpenShift Serverless
- OpenShift Pipelines

### 3-3. コンテナイメージのビルド
Tekton Pipelineを使用して、Gitリポジトリからソースコードをクローンし、コンテナイメージをビルドした上で内部レジストリへコンテナをプッシュします。

#### 3-3-1. Taskのインストール
```
tkn hub install task kaniko --version 0.6
tkn hub install task gradle --version 0.2
```

#### 3-3-2. パイプラインの作成

```
oc project bookinfo
oc apply -f manifest/tekton/pipeline
```

#### 3-3-3. パイプラインの実行
```
export NAMESPACE=bookinfo

export SERVICE_NAME=src/productpage
export IMAGE_NAME=productpage
export IMAGE_REVISION='v1'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/details
export IMAGE_NAME=details
export IMAGE_REVISION='v1'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/ratings
export IMAGE_NAME=ratings
export IMAGE_REVISION='v1'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/order
export IMAGE_NAME=order
export IMAGE_REVISION='v1'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/stock
export IMAGE_NAME=stock
export IMAGE_REVISION='v1'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/delivery
export IMAGE_NAME=delivery
export IMAGE_REVISION='v1'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/productpage-v2
export IMAGE_NAME=productpage
export IMAGE_REVISION='v2'
cat manifest/tekton/pipelinerun/pipelinerun.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/reviews
export IMAGE_NAME=reviews
export IMAGE_REVISION='v1'
export COLOR_ENABLE=false
export STAR_COLOR=""
cat manifest/tekton/pipelinerun/pipelinerun-reviews.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/reviews
export IMAGE_NAME=reviews
export IMAGE_REVISION='v2'
export COLOR_ENABLE=true
export STAR_COLOR=""
cat manifest/tekton/pipelinerun/pipelinerun-reviews.yaml | envsubst | oc apply -f -

export SERVICE_NAME=src/reviews
export IMAGE_NAME=reviews
export IMAGE_REVISION='v3'
export COLOR_ENABLE=true
export STAR_COLOR="red"
cat manifest/tekton/pipelinerun/pipelinerun-reviews.yaml | envsubst | oc apply -f -
```

### 3-4. サンプルアプリケーションのデプロイ
#### 3-4-1. Kafkaのデプロイ
```
oc project kafka
oc apply -f manifest/kafka
```

#### 3-4-2. MySQLのデプロイ
```
oc project bookinfo
oc apply -f manifest/mysql
```

#### 3-4-3. Knative Serviceのデプロイ
Knative Servingを用いてサンプルアプリケーションをデプロイします。

```
oc project bookinfo
oc apply -f manifest/knative/serving
```

#### 3-4-4. Knative Eventingの各種リソースのデプロイ

```
oc project bookinfo
oc apply -f manifest/knative/eventing/kafkasink
oc apply -f manifest/knative/eventing/broker
oc apply -f manifest/knative/eventing/source
oc apply -f manifest/knative/eventing/trigger
```

### 4. 動作確認
ブラウザ(Google Chrome推奨)を開いて`productpage`のURLへアクセスし、BookinfoのGUIが表示されることを確認してください。

![bookinfo](./img/bookinfo.png)


## 5. 演習
### 5-1. アプリケーションのデプロイ

```
kn service create productpage \
--namespace bookinfo \
--image image-registry.openshift-image-registry.svc:5000/bookinfo/productpage:v1 \
--port 9080 \
--env-from cm:bookinfo-config
```

### 5-2. オートスケール

```
ENDPOINT=$(kn service list productpage -o jsonpath='{.items[*].status.url}' )
hey -z 10s -c 700 $ENDPOINT
```
> Productpageが10台起動する

#### 5-2-1. 最小起動台数を指定

```
kn service update productpage --scale-min 1
```

### 5-3. Revisionにタグをつける

```
kn service update productpage --tag=productpage-00001=v1
```

```
kn service update reviews --tag=reviews-00001=v1
```

### 5-4. Revisionをデプロイする

#### 5-4-1. Productpage(v2)

```
kn service update productpage --image=image-registry.openshift-image-registry.svc:5000/bookinfo/productpage:v2
```

```
kn service update reviews --tag=reviews-00002=v2
```

#### 5-4-2. Reviews(v2)

```
kn service update reviews --image=image-registry.openshift-image-registry.svc:5000/bookinfo/reviews:v2
```

```
kn service update reviews --tag=reviews-00002=v2
```

#### 5-4-3. Reviews(v3)

```
kn service update reviews --image=image-registry.openshift-image-registry.svc:5000/bookinfo/reviews:v3
```

```
kn service update reviews --tag=reviews-00003=v3
```

### 5-5. トラフィック分割
#### 5-5-1. 現状のRevisionのデプロイ状況の確認
```
kn revision list
 kn revision list
NAME                SERVICE       TRAFFIC   TAGS   GENERATION   AGE     CONDITIONS   READY   REASON
delivery-00001      delivery      100%             1            2d1h    3 OK / 4     True    
details-00001       details       100%             1            2d1h    4 OK / 4     True    
order-00001         order         100%             1            2d1h    4 OK / 4     True    
productpage-00002   productpage   100%      v2     2            72s     4 OK / 4     True    
productpage-00001   productpage             v1     1            2m21s   3 OK / 4     True    
ratings-00001       ratings       100%             1            2d1h    3 OK / 4     True    
reviews-00003       reviews                 v3     3            6m52s   3 OK / 4     True    
reviews-00002       reviews                 v2     2            9m26s   3 OK / 4     True    
reviews-00001       reviews       100%      v1     1            2d1h    4 OK / 4     True    
stock-00001         stock         100%             1            2d1h    3 OK / 4     True  
```

#### 5-5-2. Reviews(v2)へ100%のトラフィックを送信(ブルーグリーンデプロイメント)
```
kn service update reviews --traffic v2=100
```

![Reviews(v2)へ100%トラフィック](./img/bg.png)

#### 5-5-3. Reviews(v2)とReviews(v3)で50%ずつトラフィックを送信(カナリアリリース)
```
kn service update reviews --traffic v2=50 --traffic v3=50
```
![Reviews(v2)へ100%トラフィック](./img/canary.png)

#### 5-5-4. Reviews(v1)が20%、Reviews(v2)とReviews(v3)が40%ずつ

```
kn service update reviews --traffic v1=20 --traffic v2=40 --traffic v3=40
```
![もっと細かい分割](./img/canary2.png)

### 5-6. Knative Eventingとの連携

Bookinfoの`[Sign in]`ボタンを押下し、任意のユーザ名 / パスワードでログインします。
その状態で、[Order]ボタンを押下し、何回かリロードした後に[Order]ボタン上のステータスが`DELIVERY_SUCCESS`へ変化すると正常動作しています。

Kafka Topicのデータは、`5. おまけ`に記載の`Kafdrop`を使用すると確認できます。(QuarkusのKafka Dev UIでもOK)

#### 5-6-1. Knative Eventingでロールバック処理を確認する
Bookinfoの`[Sign in]`ボタンを押下せずに[Order]ボタンを実行すると、本の注文処理が失敗し、Stockで管理する在庫情報がロールバックされます。
ロールバックの様子は以下で確認できます。

```
export PODNAME=$(oc get pods -l app=mysql-stock -o jsonpath='{.items[*].metadata.name}' -n bookinfo)

while true
do
oc -n bookinfo exec ${PODNAME} -- mysql -uchangeme -pchangeme -D stock -e "SELECT * FROM stock WHERE id=1" 2>/dev/null
echo "---"
sleep 1
done
```

## 6. おまけ
### 6-1. KafdropのGUIへアクセス

`kubectl proxy`でローカルへポートフォワードした上で、以下のURLへアクセスします。

```
oc project kafka
helm repo add rhcharts https://ricardo-aires.github.io/helm-charts/
helm upgrade --install aires \
--set kafka.enabled=false \
--set kafka.bootstrapServers=my-cluster-kafka-bootstrap.kafka.svc.cluster.local:9092 rhcharts/kafdrop
oc adm policy add-scc-to-user anyuid -z aires-kafdrop-sa
oc proxy
```

```
http://localhost:8001/api/v1/namespaces/kafka/services/aires-kafdrop:9000/proxy/
```

![kafdrop](./img/kafdrop.png)
